# **Riku's node-task**

![Calculator](./calculator.jpg)

## Marvelous and free tools

- [**Git**](https://git-scm.com/) - Distributed *version control*
- [**SSH**](https://en.wikipedia.org/wiki/Secure_Shell) - Secure *network protocol*
- [**npm**](https://www.npmjs.com/) - JavaScript *package manager*
- [**Node.js**](https://nodejs.org/en/) - JavaScript *runtime*
- [**Express**](https://expressjs.com/) - Node.js *web framework*

## Simple addition functionality
`const add = (a, b) => {
  const sum = a + b;
  return sum;
};`

## Usage

1. Start the app
2. Open your browser
3. Choose two numbers, **a** and **b**
4. Send a request to your server
5. *Get the sum as a response!*

### **For example:**
http://localhost:3000/add?a=2&b=3
(use your own values)

Returns:
> *5*