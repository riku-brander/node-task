const express = require("express");
const app = express();
const port = 3000;

console.log("Server-side program starting...");

app.get("/", (req, res) => {
  res.send("Hello World!");
});

/**
 * This arrow function adds two numbers together
 * @param {Number} a first parameter
 * @param {Number} b second parameter
 * @returns {Number}
 */
const add = (a, b) => {
  const sum = a + b;
  return sum;
};

// Adding endpoint http://localhost:3000/add?a=value&b=value
app.get("/add", (req, res) => {
  console.log(req.query);
  const sum = add(Number(req.query.a), Number(req.query.b));
  res.send(sum.toString());
});

app.listen(port, () => {
  console.log(`Server listening to http://localhost:${port}`);
});
